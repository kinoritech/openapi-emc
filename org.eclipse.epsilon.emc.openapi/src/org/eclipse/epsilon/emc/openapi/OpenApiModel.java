package org.eclipse.epsilon.emc.openapi;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.epsilon.common.util.StringProperties;
import org.eclipse.epsilon.eol.EolModule;
import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.exceptions.models.EolEnumerationValueNotFoundException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelElementTypeNotFoundException;
import org.eclipse.epsilon.eol.exceptions.models.EolModelLoadingException;
import org.eclipse.epsilon.eol.exceptions.models.EolNotInstantiableModelElementTypeException;
import org.eclipse.epsilon.eol.execute.introspection.IPropertyGetter;
import org.eclipse.epsilon.eol.execute.introspection.IPropertySetter;
import org.eclipse.epsilon.eol.models.CachedModel;
import org.eclipse.epsilon.eol.models.IModel;
import org.eclipse.epsilon.eol.models.IRelativePathResolver;
import org.eclipse.epsilon.eol.types.EolModelElementType;

import com.reprezen.kaizen.oasparser.OpenApi3Parser;
import com.reprezen.kaizen.oasparser.model3.OpenApi3;

public class OpenApiModel extends CachedModel<Object> implements IModel {

	public static final String PROPERTY_MODEL_FILE = "OpenApiFileProperty";

	private static String OPENAPI_PACKAGE = "com.reprezen.kaizen.oasparser.model3";
	
	private URI openApiUri;
	private File openApiFile;
	private boolean validate = true;
	
	private OpenApi3 model;
	
	
	public OpenApiModel() {
		super();
		this.propertyGetter = new OpenApiPropertyGetter();
		this.propertySetter = new OpenApiPropertySetter();
	}

	public URI getOpenApiUri() {
		return openApiUri;
	}

	public void setOpenApiUri(URI openApiUri) {
		this.openApiUri = openApiUri;
	}

	public File getOpenApiFile() {
		return openApiFile;
	}

	public void setOpenApiFile(File openApiFile) {
		this.openApiFile = openApiFile;
	}

	public boolean isValidate() {
		return validate;
	}

	public void setValidate(boolean validate) {
		this.validate = validate;
	}

	public OpenApi3 getModel() {
		return model;
	}

	public void setModel(OpenApi3 model) {
		this.model = model;
	}
	
	public static void main(String[] args) throws Exception {
		OpenApiModel openApiModel = new OpenApiModel();
		URI uri = new URI("https://raw.githubusercontent.com/OAI/OpenAPI-Specification/master/examples/v3.0/api-with-examples.yaml");
		openApiModel.setOpenApiUri(uri);
		openApiModel.load();
		
		EolModule module = new EolModule();
		module.getContext().getModelRepository().addModel(openApiModel);
		module.parse("Path.all.first.get.summary.println();");
		module.execute();
	}
	
	@Override
	public String getTypeNameOf(Object instance) {
		return instance.getClass().getName();
	}

	@Override
	public Object getElementById(String id) {
		throw new UnsupportedOperationException("getElementById");
	}

	@Override
	public void setElementId(Object instance, String newId) {
		throw new UnsupportedOperationException("setElementId");
	}

	@Override
	public boolean owns(Object instance) {
		if (instance instanceof EntryWrapper) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean hasType(String type) {
		try{
			OpenApiTypes.valueOf(type);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	protected Collection<Object> allContentsFromModel() {		
		Collection<Object> all = new ArrayList<Object>();
		for (OpenApiTypes type : OpenApiTypes.values()) {
			try{
				all.addAll(getAllOfKindFromModel(type.name()));
			} catch (Exception e) {
				// skip
			}
		}
		return all;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected Collection<Object> getAllOfTypeFromModel(String type) throws EolModelElementTypeNotFoundException {
		OpenApiTypes openApiType;
		try{
			openApiType = OpenApiTypes.valueOf(type);
		} catch (Exception e) {
			throw new EolModelElementTypeNotFoundException(this.name, type);
		}
		Object response = null;
		switch (openApiType) {
		case Callback:
			response =  EntryWrapper.wrap(model.getCallbacks(), openApiType);
			break;
		case Info:
			response = Arrays.asList(model.getInfo());
			break;
		case Example:
			response =  EntryWrapper.wrap(model.getExamples(), openApiType);
			break;
		case ExternalDocs:
			response = Arrays.asList(model.getExternalDocs());
			break;
		case Header:
			response =  EntryWrapper.wrap(model.getHeaders(), openApiType);
			break;	
		case Link:
			response = EntryWrapper.wrap( model.getLinks(), openApiType);
			break;
		case Parameter:
			response =  EntryWrapper.wrap(model.getParameters(), openApiType);
			break;
		case Path:
			response =  EntryWrapper.wrap(model.getPaths(), openApiType);
			break;
		case RequestBody:
			response =  EntryWrapper.wrap(model.getRequestBodies(), openApiType);
			break;
		case Response:
			response =  EntryWrapper.wrap(model.getResponses(), openApiType);
			break;
		case Schema:
			response = EntryWrapper.wrap(model.getSchemas(), openApiType);
			break;
		case SecurityScheme:
			response =  EntryWrapper.wrap(model.getSecuritySchemes(), openApiType);
			break;
		case Server:
			response = model.getServers();
			break;
		case Tag:
			response = model.getTags();
			break;
		default:
			response = Collections.emptyList();
			break;
		}
		return (Collection<Object>) response;
	}
	
	@Override
	protected Collection<Object> getAllOfKindFromModel(String kind) throws EolModelElementTypeNotFoundException {
		return getAllOfTypeFromModel(kind);
	}


	@Override
	protected void loadModel() throws EolModelLoadingException {
		if (getOpenApiFile() != null) {
			try {
				model = (OpenApi3) new OpenApi3Parser().parse(getOpenApiFile(), validate);
			} catch (Exception e) {
				throw new EolModelLoadingException(e, this);
			}
		} else if (getOpenApiUri() != null) {
			try {
				model = (OpenApi3) new OpenApi3Parser().parse(getOpenApiUri(), validate);
			} catch (Exception e) {
				throw new EolModelLoadingException(e, this);
			}
		} else {
			Exception ex = new Exception("No model file or url provided");
			throw new EolModelLoadingException(ex, this);
		}
	}
	
	@Override
	public void load(StringProperties properties, IRelativePathResolver resolver) throws EolModelLoadingException {
		super.load(properties, resolver);
		openApiFile = new File(resolver.resolve(properties.getProperty(PROPERTY_MODEL_FILE)));
		load();
	}

	
	@Override
	protected Object getCacheKeyForType(String type) throws EolModelElementTypeNotFoundException {
		return type;
	}
	
	@Override
	protected Collection<String> getAllTypeNamesOf(Object instance) {
		return Arrays.asList(instance.getClass().getName());
	}
	
	
	@Override
	public boolean store(String location) {
		return false; 
	}

	@Override
	public boolean store() {
		return false;
	}
	
	@Override
	protected void disposeModel() { }

	// FIXME
	@Override
	public boolean isInstantiable(String type) {
		return false; 
	}

	// FIXME
	@Override
	protected Object createInstanceInModel(String type)
			throws EolModelElementTypeNotFoundException, EolNotInstantiableModelElementTypeException {
		return null;
	}

	// FIXME
	@Override
	protected boolean deleteElementInModel(Object instance) throws EolRuntimeException {
		return false; 
	}
	
	@Override
	public String getElementId(Object instance) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Object getEnumerationValue(String enumeration, String label) throws EolEnumerationValueNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

}
