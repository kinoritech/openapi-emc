package org.eclipse.epsilon.emc.openapi;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

public class EntryWrapper {

	protected Map.Entry<String, ?> entry;
	protected OpenApiTypes type;
	
	public EntryWrapper(Map.Entry<String, ?> entry, OpenApiTypes type) {
		this.entry = entry;
		this.type = type;
	}
	
	public String getName() {
		return this.entry.getKey();
	};
	
	public Object getContents(){
		return this.entry.getValue();
	}
	
	public String getType(){
		return this.type.name();
	}
	
	public static Collection<EntryWrapper> wrap(Map<String, ?> entries, OpenApiTypes type) {
		return entries.entrySet().stream().map(m-> new EntryWrapper(m, type)).collect(Collectors.toList());
	}
	
	@Override
	public String toString() {
		return type.name() + ": [name = " + getName() + "]";
	}
}
