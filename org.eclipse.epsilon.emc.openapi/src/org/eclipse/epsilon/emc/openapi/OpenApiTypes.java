package org.eclipse.epsilon.emc.openapi;

// OpenApiType#Callback
public enum OpenApiTypes {
	Callback,
	Contact,
	Discriminator,
	EncodingProperty,
	Example,
	ExternalDocs,
	Header,
	Info,
	License,
	Link,
	MediaType,
	OAuthFlow,
	OpenApi3,
	Operation,
	Parameter,
	Path,
	RequestBody,
	Response,
	Schema,
	SecurityParameter,
	SecurityRequirement,
	SecurityScheme,
	Server,
	ServerVariable,
	Tag,
	Xml
	;
}
