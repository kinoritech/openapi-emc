package org.eclipse.epsilon.emc.openapi;

import org.eclipse.epsilon.eol.exceptions.EolRuntimeException;
import org.eclipse.epsilon.eol.execute.context.IEolContext;
import org.eclipse.epsilon.eol.execute.introspection.java.JavaPropertyGetter;

public class OpenApiPropertyGetter extends JavaPropertyGetter{

	
	@Override
	public Object invoke(Object object, String property) throws EolRuntimeException {
		if (object instanceof EntryWrapper) {
			if (!property.equalsIgnoreCase("name")) {
				EntryWrapper wrapper = (EntryWrapper) object; 
				super.invoke(wrapper.getContents(), property);
			}
		}
		return super.invoke(object, property);
	}
	
	@Override
	public Object invoke(Object object, String property, IEolContext context) throws EolRuntimeException {
		if (object instanceof EntryWrapper) {
			if (!property.equalsIgnoreCase("name")) {
				EntryWrapper wrapper = (EntryWrapper) object; 
				return super.invoke(wrapper.getContents(), property, context);
			}
		}
		return super.invoke(object, property, context);
	}
	
}
