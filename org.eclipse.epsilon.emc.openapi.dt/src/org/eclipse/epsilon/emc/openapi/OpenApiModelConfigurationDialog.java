package org.eclipse.epsilon.emc.openapi;

import org.eclipse.epsilon.common.dt.launching.dialogs.AbstractCachedModelConfigurationDialog;
import org.eclipse.epsilon.common.dt.util.DialogUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

public class OpenApiModelConfigurationDialog extends AbstractCachedModelConfigurationDialog {

	private Text modelFileText;

	@Override
	protected String getModelName() {
		return "OpenAPI model";
	}

	@Override
	protected String getModelType() {
		return "OpenAPI";
	}

	@Override
	protected void loadProperties() {
		super.loadProperties();
		if (properties == null)
			return;
		modelFileText.setText(properties.getProperty(OpenApiModel.PROPERTY_MODEL_FILE));
	}

	@Override
	protected void storeProperties() {
		super.storeProperties();
		properties.put(OpenApiModel.PROPERTY_MODEL_FILE, modelFileText.getText());
	}

	@Override
	protected void createGroups(Composite control) {
		super.createGroups(control);
		createFileGroup(control);
		createLoadStoreOptionsGroup(control);
	}
	
	protected Composite createFileGroup(Composite parent) {
		final Composite groupContent = DialogUtil.createGroupContainer(parent, "Files", 3);
		((GridData) groupContent.getParent().getLayoutData()).grabExcessVerticalSpace = true;

		final Label modelFileLabel = new Label(groupContent, SWT.NONE);
		modelFileLabel.setText("OpenAPI (YAML) file: ");

		modelFileText = new Text(groupContent, SWT.BORDER);
		modelFileText.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		final Button browseModelFile = new Button(groupContent, SWT.NONE);
		browseModelFile.setText("Browse Workspace...");
		browseModelFile.addListener(SWT.Selection, new BrowseWorkspaceForModelsListener(modelFileText,
				"OpenAPI models in the workspace", "Select an OpenAPI model"));

		return groupContent;
	}
}
